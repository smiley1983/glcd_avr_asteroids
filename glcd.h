#ifndef _GLCD_H_
#define _GLCD_H_
void print_string_inverse(uint8_t col, uint8_t row, char s[]);
void write_gfx(uint8_t segment, uint8_t gfx[]);
void print_string(uint8_t col,uint8_t row,char s[]);
void setup(void);
void setup_port_dirs(void);
void set_start_line(unsigned short line);
void glcd_blk(void);
void glcd_clr(void);
void glcd_blkln(unsigned short ln);
void glcd_clrln(unsigned short ln);
void put_char(uint8_t row,uint8_t col,uint8_t character);
void propagate(unsigned short x,unsigned short y);
void draw_point(unsigned short x,unsigned short y,unsigned short color);
unsigned short glcd_read(unsigned short column);
void glcd_write(unsigned short b);
void goto_xy(unsigned int x,unsigned int y);
void goto_row(unsigned int y);
void goto_col(unsigned int x);
void glcd_off(void);
void glcd_on(void);
void enable_pulse(void);
void en_high(void);
void en_low(void);
void rst_high(void);
void rst_low(void);
void rw_high(void);
void rw_low(void);
void rs_high(void);
void rs_low(void);
void csb_high(void);
void csb_low(void);
void csa_high(void);
void csa_low(void);
#endif
