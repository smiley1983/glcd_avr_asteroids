
This project contains:

**
glcd.c
glcd.h

Basic routines for communicating with a ks0108-based GLCD module (in my case a GXM19264-01). These routines were ported from this Embedded Lab:

http://embedded-lab.com/blog/?p=2398

**
button.c, button.h:

for reading button states

**
game.c
game.h

asteroids game

**
cp437font8x8.h

codepage 437 font data

**
asteroids.c

ties it all together

