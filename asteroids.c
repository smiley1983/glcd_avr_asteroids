#include<avr/io.h>
#include "button.h"
#include "game.h"
#include "glcd.h"

#define F_CPU 8000000
#include<util/delay.h>


#define display_width 192
#define display_height 64
#define gfx_width 64
#define gfx_height 64


void menu(void) {
  print_string (3,1, "GLCD AVR ASTEROIDS");
  print_string_inverse (4,4, "Press any button");
  print_string (1,7, "Made by Jude Hungerford");
  _delay_ms(700);
  uint8_t more = 1;
  while(more) {
    if (button1_pressed() || button2_pressed() || button3_pressed() || button4_pressed() || button5_pressed() || button0_pressed()) {
      play_game();
      more = 0;
    }
  }
}

int main(void) {

  setup();
  init_buttons();

  while(1) {
    glcd_clr();
    menu();
  }
}


