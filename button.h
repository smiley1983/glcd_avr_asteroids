#ifndef _BUTTON_H_
#define _BUTTON_H_

uint8_t button1_pressed(void);
uint8_t button_pressed(uint8_t *b);
void init_buttons(void);
void reset_clock(void);
extern uint16_t repeat_delay;
extern uint16_t bounce_delay;
extern uint8_t button1;
uint8_t button0_pressed(void);
uint8_t button1_pressed(void);
uint8_t button2_pressed(void);
uint8_t button3_pressed(void);
uint8_t button4_pressed(void);
uint8_t button5_pressed(void);
#endif
