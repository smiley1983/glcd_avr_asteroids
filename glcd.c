#include<avr/io.h>
// this is the header file that tells the compiler what pins and ports, etc.
// are available on this chip.
#define F_CPU 8000000
#include<util/delay.h>
#include <avr/pgmspace.h>
#include<cp437font8x8.h>

// Some macros that make the code more readable
#define output_low(port,pin) port &= ~(1<<pin)
#define output_high(port,pin) port |= (1<<pin)
#define set_input(portdir,pin) portdir &= ~(1<<pin)
#define set_output(portdir,pin) portdir |= (1<<pin)

#define CSA PC0
#define CSB PC1
#define RST PC5

#define LED PB0

#define EN PC4
#define RS PC2
#define RW PC3
#define DATA PORTD

#define display_width 192
#define display_height 64

void setup_port_dirs(void) {
  set_output(DDRC, CSA);
  set_output(DDRC, CSB);
  set_output(DDRC, RST);
  set_output(DDRC, RS);
  set_output(DDRC, RW);
  set_output(DDRC, EN);
  
  DDRD=0xFF;
  DATA=0x00;

}

void csa_low(void)
{
    output_low(PORTC, CSA);
}

void csa_high(void)
{
    output_high(PORTC, CSA);
}

void csb_low(void)
{
    output_low(PORTC, CSB);
}

void csb_high(void)
{
    output_high(PORTC, CSB);
}

void rs_low(void)
{
    output_low(PORTC, RS);
}

void rs_high(void)
{
    output_high(PORTC, RS);
}

void rw_low(void)
{
    output_low(PORTC, RW);
}

void rw_high(void)
{
    output_high(PORTC, RW);
}

void rst_low(void)
{
    output_low(PORTC, RST);
}

void rst_high(void)
{
    output_high(PORTC, RST);
}

void en_low(void)
{
    output_low(PORTC, EN);
}

void en_high(void)
{
    output_high(PORTC, EN);
}

void enable_pulse(void)
{
  en_high();
  _delay_us(5);
  en_low();
  _delay_us(5);
}

void glcd_on(void)
{
    csa_low();
    csb_low();
    rs_low();
    rw_low();
    DATA = 0x3F;         //ON command
    enable_pulse();
    csb_high();
    enable_pulse();
    csa_high();
    csb_low();
    enable_pulse();
}

void glcd_off(void)
{
    //FIXME this function should probably send a separate OFF command to each
    //chip, same as glcd_on does.
    csa_high();
    csb_low();
    rs_low();
    rw_low();
    DATA = 0x3E;         //OFF command
    enable_pulse();
}

void goto_col(unsigned int x)
{
   unsigned short Col_Data;
   rs_low();
   rw_low();
   if(x<64)             //left section
   {
      csa_low();
      csb_low();
      Col_Data = x;              //put column address on data port
   }
   else if (x<128)                //middle
   {
      csa_low();
      csb_high();
      Col_Data = x-64;   //put column address on data port
   }
   else                 //right section
   {
      csa_high();
      csb_low();
      Col_Data = x-128;   //put column address on data port
   }
   Col_Data = (Col_Data | 0x40 ) & 0x7F;  //Command format
   DATA = Col_Data;
   enable_pulse();
}
 
void goto_row(unsigned int y)
{
   unsigned short Col_Data;
   rs_low();
   rw_low();
   Col_Data = (y | 0xB8 ) & 0xBF; //put row address on data port set command
   DATA = Col_Data;
   enable_pulse();
}

void goto_xy(unsigned int x,unsigned int y)
{
    goto_col(x);
    goto_row(y);
}

void glcd_write(unsigned short b)
{
   rs_high();
   rw_low();
   DATA = b;            //put data on data port
   _delay_us(1);
   enable_pulse();
}
 
void put_char(uint8_t row, uint8_t col, uint8_t character) {
  uint16_t index = character * 8;
  goto_xy (col * 8, row);
  uint8_t i;
  for (i=0;i<8;i++) {
    glcd_write(pgm_read_byte(&cp437[index + i]));
  }
}

void put_char_inverse(uint8_t row, uint8_t col, uint8_t character) {
  uint16_t index = character * 8;
  goto_xy (col * 8, row);
  uint8_t i;
  for (i=0;i<8;i++) {
    glcd_write(~pgm_read_byte(&cp437[index + i]));
  }
}

void glcd_clrln(unsigned short ln)
{
   int i;
   goto_xy(0,ln);      //At start of line of left side
   csa_low();
   csb_low();
   for(i=0;i<65;i++)
     glcd_write(0x00);

   goto_xy(64,ln);     //At start of line of right side
   csa_high();
   csb_low();
   for(i=0;i<65;i++)
     glcd_write(0x00);

   goto_xy(128,ln);     //At start of line of right side
   csa_low();
   csb_high();
   for(i=0;i<65;i++)
     glcd_write(0x00);
}

void glcd_blkln(unsigned short ln)
{
   int i;
   goto_xy(0,ln);      //At start of line of left side
   csa_low();
   csb_low();
   for(i=0;i<65;i++)
     glcd_write(0xff);

   goto_xy(64,ln);     //At start of line of right side
   csa_high();
   csb_low();
   for(i=0;i<65;i++)
     glcd_write(0xff);

   goto_xy(128,ln);     //At start of line of right side
   csa_low();
   csb_high();
   for(i=0;i<65;i++)
     glcd_write(0xff);
}

void glcd_clr(void)
{
   unsigned short m;
   for(m=0;m<8;m++){
    glcd_clrln(m);
   }
}

void glcd_blk(void)
{
   unsigned short m;
   for(m=0;m<8;m++){
    glcd_blkln(m);
   }
}

void set_start_line(unsigned short line)
{
    rs_low();
    rw_low();
    //Activate both chips
    csa_low();
    csb_low();
    DATA = 0xC0 | line;     //Set Start Line command
    enable_pulse();
}

void print_string(uint8_t col, uint8_t row, char s[]) {
  uint8_t finished = 0;
  uint8_t count = 0;
  while (!finished) {
    if (s[count] == '\0') finished = 1;
    else {
      goto_xy (col*8, row);
      put_char(row, col, s[count]);
      count++;
      col++;
      if (col >= display_width / 8) {
        col = 0;
	row++;
      }
    }
  }
}

void print_string_inverse(uint8_t col, uint8_t row, char s[]) {
  uint8_t finished = 0;
  uint8_t count = 0;
  while (!finished) {
    if (s[count] == '\0') finished = 1;
    else {
      goto_xy (col*8, row);
      put_char_inverse(row, col, s[count]);
      count++;
      col++;
      if (col >= display_width / 8) {
        col = 0;
	row++;
      }
    }
  }
}

void setup(void) {
  setup_port_dirs();
  rst_low();
  enable_pulse();
  _delay_ms(15);

  rst_high();

  csa_high();
  csb_high();
  _delay_ms(1000);
  glcd_on();
  _delay_ms(1000);
  glcd_clr();
}

void write_gfx(uint8_t segment, uint8_t gfx[]) {
  uint8_t i = 0;
  uint8_t j = 0;
  uint16_t index = 0;
//  for(i=0;i<sizeof(gfx);i++) {
  for(i=0; i<8; i++) {
    goto_xy((segment * 64), i);
    for(j=0;j<64;j++) {
      index = i * 64 + j;
      glcd_write(gfx[index]);
    }
  }
}

