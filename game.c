#include<avr/io.h>
#include<stdlib.h>
//#include<stdio.h>
#include <avr/pgmspace.h>
#include "glcd.h"
#include "button.h"
#include "smallmath.h"

#define F_CPU 8000000
#include<util/delay.h>


#define gfx_width 64
#define gfx_height 64

#define max_bullets 16
#define len_bullet 32

#define max_asteroids 32
#define len_asteroid 64

#define set_fire_delay 40

#define half_circle 63
#define degrees 126
#define degree_bound 127
#define velocity_scale 128

uint8_t asteroid_count = 0;

void init_uint8_t_array(uint8_t arr[], uint16_t size, uint8_t val) {
  unsigned long i = 0;
  for (i=0; i<size;i++) { arr[i] = val; }
}

void init_int8_t_array(int8_t arr[], uint16_t size, int8_t val) {
  unsigned long i = 0;
  for (i=0; i<size;i++) { arr[i] = val; }
}

void init_uint16_t_array(uint16_t arr[], uint16_t size, uint16_t val) {
  unsigned long i = 0;
  for (i=0; i<size;i++) { arr[i] = val; }
}

//void update(g) {
  
//}

uint8_t bound(uint8_t v, uint8_t b) {
  return v % b;;
}

void mark_bullet(uint8_t gfxbuf[], uint16_t bullet[], uint8_t bullet_lifetime[]) {
  uint8_t i = 0;
  for (i=0; i<len_bullet; i+=2) {
    if (bullet_lifetime[i/2]) {
      uint8_t x = bound(bullet[i] / 256, 64);
      uint8_t y = bound(bullet[i+1] / 256, 64);
      uint8_t row = y / 8;
      uint8_t bit = y % 8;
      gfxbuf[x + (row * gfx_width)] &= ~(1 << bit);
    }
  }
}

void mark_asteroid(uint8_t gfxbuf[], uint16_t asteroid[], uint8_t asteroid_diameter[]) {
  uint8_t i = 0;
  uint8_t c1 = 0;
  uint8_t c2 = 0;
  for (i=0; i<len_asteroid; i+=2) {
    if (asteroid_diameter[i/2]) {
      uint8_t ad = asteroid_diameter[i/2];
      for(c1=0;c1<asteroid_diameter[i/2];c1++) {
        for(c2=0;c2<asteroid_diameter[i/2];c2++) {
	  int8_t d1 = c1 - ad / 2;
	  int8_t d2 = c2 - ad / 2;
	  if(d1 * d1 + d2 * d2 < (ad / 2) * (ad / 2)) {
            uint8_t x = bound(asteroid[i] / 256 + c1, 64);
            uint8_t y = bound(asteroid[i+1] / 256 + c2, 64);
            uint8_t row = y / 8;
            uint8_t bit = y % 8;
            gfxbuf[x + (row * gfx_width)] &= ~(1 << bit);
	  }
        }
      }
    }
  }
}

void push_bullets(uint16_t bullet[], int8_t bullet_velocity[], uint8_t bullet_lifetime[]) {
  int8_t i;
  for (i=len_bullet-1;i>0;i-=2) {
    bullet[i] = bullet[i-2];
    bullet[i-1] = bullet[i-3];
    bullet_velocity[i] = bullet_velocity[i-2];
    bullet_velocity[i-1] = bullet_velocity[i-3];
    bullet_lifetime[i/2] = bullet_lifetime[i/2-1];
  }
}

void add_bullet(uint8_t x, uint8_t y, int8_t xvel, int8_t yvel, uint16_t bullet[], int8_t bullet_velocity[], uint8_t bullet_lifetime[], uint8_t lifetime) {
  push_bullets(bullet, bullet_velocity, bullet_lifetime);
  bullet[0] = x * 256;
  bullet[1] = y * 256;
  bullet_lifetime[0] = lifetime;
  bullet_velocity[0] = xvel;
  bullet_velocity[1] = yvel;
}

void add_asteroid(uint8_t x, uint8_t y, int8_t xvel, int8_t yvel, uint16_t bullet[], int8_t bullet_velocity[], uint8_t bullet_lifetime[], uint8_t lifetime) {
  bullet[asteroid_count * 2] = x * 256;
  bullet[asteroid_count * 2 + 1] = y * 256;
  bullet_lifetime[asteroid_count] = lifetime;
  bullet_velocity[asteroid_count * 2] = xvel;
  bullet_velocity[asteroid_count * 2 + 1] = yvel;
  asteroid_count++;
}

uint8_t collide_asteroid(uint16_t cx, uint16_t cy, int8_t cxvel, int8_t cyvel, uint16_t asteroid[], int8_t asteroid_velocity[], uint8_t asteroid_diameter[]) {
  uint8_t i = 0;
  uint8_t c1 = 0;
  uint8_t c2 = 0;
  uint8_t collided = 0;
  for (i=0; i<len_asteroid; i+=2) {
    if (asteroid_diameter[i/2]) {
      uint8_t ad = asteroid_diameter[i/2];
      for(c1=0;c1<asteroid_diameter[i/2];c1++) {
        for(c2=0;c2<asteroid_diameter[i/2];c2++) {
	  int8_t d1 = c1 - ad / 2;
	  int8_t d2 = c2 - ad / 2;
	  if(d1 * d1 + d2 * d2 < (ad / 2) * (ad / 2)) {
            uint8_t x = bound(asteroid[i] / 256 + c1, 64);
            uint8_t y = bound(asteroid[i+1] / 256 + c2, 64);
	    if ((x == cx / 256) && (y == cy / 256)) {
              uint8_t px = asteroid[i] / 256 + 1;
	      uint8_t py = asteroid[i+1] / 256 + 1;
	      int8_t xv = asteroid_velocity[i];
	      int8_t yv = asteroid_velocity[i+1];
	      int8_t diam = asteroid_diameter[i/2]-4;
              if (diam > 3) {
                add_asteroid(px, py, xv - cxvel/2, yv - cxvel/2, asteroid, asteroid_velocity, asteroid_diameter, diam);
              }

	      asteroid_diameter[i/2]-=4;
	      asteroid[i] += asteroid_diameter[i/2]/2;
	      asteroid[i+1] += asteroid_diameter[i/2]/2;
	      asteroid_velocity[i] += cxvel/2;
	      asteroid_velocity[i+1] += cyvel/2;



	      collided++;
	    }
	  }
        }
      }
    }
  }
  return collided;
}

void mark_point(uint8_t gfxbuf[], uint8_t x, uint8_t y, uint8_t value) {
  uint8_t row = y / 8;
  uint8_t bit = y % 8;
  if (value) 
    gfxbuf[x + (row * gfx_width)] |= (1 << bit);
  else 
    gfxbuf[x + (row * gfx_width)] &= ~(1 << bit);
}

uint8_t point_occupied(uint8_t gfxbuf[], uint8_t x, uint8_t y) {
  uint8_t row = y / 8;
  uint8_t bit = y % 8;
  return ((gfxbuf[x + (row * gfx_width)] & (1<<bit)) == 0);
}


uint8_t gen_mark_ship (uint8_t gfxbuf[], uint16_t x, uint16_t y, uint8_t facing, uint8_t value) {
  int8_t lx = pgm_read_byte(&mcos[(facing + 54)%degree_bound]);
  int8_t ly = pgm_read_byte(&msin[(facing + 54)%degree_bound]);
  int8_t rx = pgm_read_byte(&mcos[(facing + 72)%degree_bound]);
  int8_t ry = pgm_read_byte(&msin[(facing + 72)%degree_bound]);
  int8_t dx = pgm_read_byte(&mcos[facing]);
  int8_t dy = pgm_read_byte(&msin[facing]);
  uint8_t i;
  uint8_t result = 0;
  for (i=0;i<4;i++) {
    uint8_t px = bound(x/256 + ((dx * i) / velocity_scale), 64); 
    uint8_t py = bound(y/256 + ((dy * i) / velocity_scale), 64);
    if (point_occupied (gfxbuf, px, py)) result = 1;
    px = bound(x/256 + ((lx * i) / velocity_scale), 64); 
    py = bound(y/256 + ((ly * i) / velocity_scale), 64);
    if (point_occupied (gfxbuf, px, py)) result = 1;
    px = bound(x/256 + ((rx * i) / velocity_scale), 64); 
    py = bound(y/256 + ((ry * i) / velocity_scale), 64);
    if (point_occupied (gfxbuf, px, py)) result = 1;
  }
  for (i=0;i<4;i++) {
    uint8_t px = bound(x/256 + ((dx * i) / velocity_scale), 64); 
    uint8_t py = bound(y/256 + ((dy * i) / velocity_scale), 64);
    mark_point (gfxbuf, px, py, value);
    px = bound(x/256 + ((lx * i) / velocity_scale), 64); 
    py = bound(y/256 + ((ly * i) / velocity_scale), 64);
    mark_point (gfxbuf, px, py, value);
    px = bound(x/256 + ((rx * i) / velocity_scale), 64); 
    py = bound(y/256 + ((ry * i) / velocity_scale), 64);
    mark_point (gfxbuf, px, py, value);
  }
  return result;
}

uint8_t mark_ship (uint8_t gfxbuf[], uint16_t x, uint16_t y, uint8_t facing) {
  return gen_mark_ship(gfxbuf, x, y, facing, 0);
}

void unmark_ship(uint8_t gfxbuf[], uint16_t x, uint16_t y, uint8_t facing) {
  gen_mark_ship(gfxbuf, x, y, facing, 1);
}

void unmark_bullet(uint8_t gfxbuf[], uint16_t bullet[], uint8_t bullet_lifetime[]) {
  uint8_t i = 0;
  for (i=0; i<len_bullet; i+=2) {
    if (bullet_lifetime[i/2]) {
      uint8_t x = bound(bullet[i] / 256, 64);
      uint8_t y = bound(bullet[i+1] / 256, 64);
      uint8_t row = y / 8;
      uint8_t bit = y % 8;
      gfxbuf[x + (row * gfx_width)] |= (1 << bit);
    }
  }
}

void unmark_asteroid(uint8_t gfxbuf[], uint16_t asteroid[], uint8_t asteroid_diameter[]) {
  uint8_t i = 0;
  uint8_t c1 = 0;
  uint8_t c2 = 0;
  for (i=0; i<len_asteroid; i+=2) {
    if (asteroid_diameter[i/2]) {
      for(c1=0;c1<asteroid_diameter[i/2];c1++) {
        for(c2=0;c2<asteroid_diameter[i/2];c2++) {
          uint8_t x = bound(asteroid[i] / 256 + c1, 64);
          uint8_t y = bound(asteroid[i+1] / 256 + c2, 64);
          uint8_t row = y / 8;
          uint8_t bit = y % 8;
          gfxbuf[x + (row * gfx_width)] |= (1 << bit);
        }
      }
    }
  }
}

void update_pos (uint16_t *x, uint16_t *y, int16_t *xvel, int16_t *yvel) {
  if (*xvel < 0) {
    if (*x < abs(*xvel)) (*x) += 16384;
  }
  if (*yvel < 0) {
    if (*y < abs(*yvel)) (*y) += 16384;
  }
  (*x) += (*xvel / velocity_scale);
  (*y) += (*yvel / velocity_scale);
  if (*x > 16384) (*x) -= 16384;
  if (*y > 16384) (*y) -= 16384;
}

void update_bullets(uint16_t bullet[], int8_t bullet_velocity[], uint8_t bullet_lifetime[], uint8_t shrink) {
  uint8_t i = 0;
  for (i=0; i<len_bullet; i+=2) {
    if (bullet_lifetime[i/2] > 0) {
      if (bullet_velocity[i] < 0) {
        if (bullet[i] < abs(bullet_velocity[i])) bullet[i] += 16384;
      }
      if (bullet_velocity[i+1] < 0) {
        if (bullet[i+1] < abs(bullet_velocity[i+1])) bullet[i+1] += 16384;
      }
      bullet[i] += bullet_velocity[i];
      bullet[i+1] += bullet_velocity[i+1];
      if (bullet[i] >= 16384) bullet[i] -= 16384;
      if (bullet[i+1] >= 16384) bullet[i+1] -= 16384;
      if (shrink) bullet_lifetime[i/2]--;
    }
  }
}

uint8_t left_button (void) { return button0_pressed(); }
uint8_t right_button (void) { return button3_pressed(); }
uint8_t up_button (void) { return button1_pressed(); }
uint8_t down_button (void) { return button2_pressed(); }
uint8_t a_button (void) { return button5_pressed(); }
uint8_t b_button (void) { return button4_pressed(); }

void turn_left (uint8_t *facing) {
  if (*facing == 0) *facing = degrees;
  else (*facing)--;
}

void turn_right (uint8_t *facing) {
  (*facing)++;
  (*facing)%=degree_bound;
}

void accelerate(uint16_t *x, uint16_t *y, int16_t *xvel, int16_t *yvel, uint8_t *facing) {
  int8_t dx = pgm_read_byte(&mcos[*facing]);
  int8_t dy = pgm_read_byte(&msin[*facing]);
  (*xvel) += dx * 2;
  (*yvel) += dy * 2;
  if (*xvel > 8192) (*xvel) = 8192;
  if (*yvel > 8192) (*yvel) = 8192;
  if (*xvel < -8192) (*xvel) = -8192;
  if (*yvel < -8192) (*yvel) = -8192;
}

void reverse(uint16_t *x, uint16_t *y, int16_t *xvel, int16_t *yvel, uint8_t *facing) {
  int8_t dx = pgm_read_byte(&mcos[((*facing) + half_circle) % degree_bound]);
  int8_t dy = pgm_read_byte(&msin[((*facing) + half_circle) % degree_bound]);
  (*xvel) += dx;
  (*yvel) += dy;
  if (*xvel > 8192) (*xvel) = 8192;
  if (*yvel > 8192) (*yvel) = 8192;
  if (*xvel < -8192) (*xvel) = -8192;
  if (*yvel < -8192) (*yvel) = -8192;
}

void fire (uint16_t *x, uint16_t *y, int16_t *xvel, int16_t *yvel, uint8_t *facing, uint16_t bullet[], int8_t bullet_velocity[], uint8_t bullet_lifetime[], uint8_t *fire_delay) {
  if (*fire_delay == 0) {
    int8_t tcos = pgm_read_byte(&mcos[*facing]);
    int8_t tsin = pgm_read_byte(&msin[*facing]);
    int16_t dx = tcos * 127;
    int16_t dy = tsin * 127;
    add_bullet((*x) / 256, (*y) / 256, ((*xvel) + dx) / 256, ((*yvel) + dy) / 256, bullet, bullet_velocity, bullet_lifetime, 210);
    *fire_delay = set_fire_delay;
  }
}
 
void pause (void) {
  uint8_t more = 1;
  print_string (9, 3, "paused");
  while(more) {
    _delay_ms(100);
    if (button1_pressed() || button2_pressed() || button3_pressed() || button4_pressed() || button5_pressed() || button0_pressed()) {
      more = 0;
    }
  }
}

void message (char s[]) {
  uint8_t more = 1;
  print_string (9, 3, s);
  _delay_ms(1200);
  while(more) {
    if (button1_pressed() || button2_pressed() || button3_pressed() || button4_pressed() || button5_pressed() || button0_pressed()) {
      more = 0;
    }
  }
}

void victory_message (char s[]) {
  uint8_t more = 1;
  print_string (8, 3, s);
  _delay_ms(1200);
  while(more) {
    if (button1_pressed() || button2_pressed() || button3_pressed() || button4_pressed() || button5_pressed() || button0_pressed()) {
      more = 0;
    }
  }
}

void noconfirm_message (char s[]) {
  print_string (8, 3, s);
  _delay_ms(1500);
}

void level_message (char s[]) {
  print_string (9, 3, "level");
  print_string (11, 4, s);
  _delay_ms(1500);
  print_string (17, 0, "level");
  print_string (19, 1, s);
}

void update_ship(uint16_t *x, uint16_t *y, int16_t *xvel, int16_t *yvel, uint8_t *facing, uint16_t bullet[], int8_t bullet_velocity[], uint8_t bullet_lifetime[], uint8_t *fire_delay) {
  if (left_button()) turn_left(facing);
  if (right_button()) turn_right(facing);
  if (up_button()) accelerate(x, y, xvel, yvel, facing);
  if (down_button()) reverse(x, y, xvel, yvel, facing);
  if (a_button()) fire(x, y, xvel, yvel, facing, bullet, bullet_velocity, bullet_lifetime, fire_delay);
  if (b_button()) pause();
  if (*fire_delay > 0) (*fire_delay)--;
  update_pos(x, y, xvel, yvel);
}

uint8_t count_asteroids(uint8_t a[]) {
  uint8_t active = 0;
  uint8_t i;
  for(i=0;i<max_asteroids;i++) {
    if (a[i] > 0) active++;
  }
  return active;
}

void collide_bullets (uint16_t bullet[], int8_t bullet_velocity[], uint8_t bullet_lifespan[], uint16_t asteroid[], int8_t asteroid_velocity[], uint8_t asteroid_diameter[]) {
  uint8_t i;
  for (i=0;i<len_bullet;i+=2) {
    if (bullet_lifespan[i/2]>0) {
      if (collide_asteroid(bullet[i], bullet[i+1], bullet_velocity[i], bullet_velocity[i+1], asteroid, asteroid_velocity, asteroid_diameter)) bullet_lifespan[i/2]=0;
    }
  }
}

void collisions (uint16_t ship_x, uint16_t ship_y, uint16_t bullet[], int8_t bullet_velocity[], uint8_t bullet_lifespan[], uint16_t asteroid[], int8_t asteroid_velocity[], uint8_t asteroid_diameter[]) {
  collide_bullets (bullet, bullet_velocity, bullet_lifespan, asteroid, asteroid_velocity, asteroid_diameter);
}

void launch_level (uint16_t asteroid[], int8_t asteroid_velocity[], uint8_t asteroid_diameter[], uint8_t level) {
  asteroid_count = 0;
  if (level < 2) {
    add_asteroid(40, 16, 3, -12, asteroid, asteroid_velocity, asteroid_diameter, 12);
    level_message ("1");
  }
  else if (level == 2) {
    add_asteroid(16, 40, -28, 14, asteroid, asteroid_velocity, asteroid_diameter, 16);
    level_message ("2");
  }
  else if (level == 3) {
    add_asteroid(40, 16, 3, -12, asteroid, asteroid_velocity, asteroid_diameter, 16);
    add_asteroid(16, 40, -28, 14, asteroid, asteroid_velocity, asteroid_diameter, 12);
    level_message ("3");
  }
  else if (level == 4) {
    add_asteroid(40, 16, 3, -12, asteroid, asteroid_velocity, asteroid_diameter, 16);
    add_asteroid(40, 16, -7, 16, asteroid, asteroid_velocity, asteroid_diameter, 16);
    level_message ("4");
  }
/*
 8switch (level) {
    case 1:
      add_bullet(40, 16, 3, -12, asteroid, asteroid_velocity, asteroid_diameter, 16);
      noconfirm_message ("level 1");
      break;
    case 2:
      noconfirm_message ("level 2");
      break;
    case 3:
      noconfirm_message ("level 3");
      break;
    case 4:
      noconfirm_message ("level 4");
      break;
    case 5:
      noconfirm_message ("level 5");
      break;
    case 6:
      noconfirm_message ("level 6");
      break;
    case 7:
      noconfirm_message ("level 7");
      break;
    default:
      noconfirm_message ("level 8");
  }
*/
}
  uint8_t o_gfxbuf[512];

  uint16_t o_bullet[len_bullet];
  int8_t o_bullet_velocity[len_bullet];
  uint8_t o_bullet_lifetime[max_bullets];

  uint16_t o_asteroid[len_asteroid];
  int8_t o_asteroid_velocity[len_asteroid];
  uint8_t o_asteroid_diameter[max_asteroids];

void play_game(void) {
  uint8_t o_facing = 12;
  uint16_t o_ship_x = 32 * 256;
  uint16_t o_ship_y = 32 * 256;
  int16_t o_ship_xvel = 0;
  int16_t o_ship_yvel = 0;

  uint8_t o_fire_delay = 0;
  uint16_t o_timer = 0;
  uint8_t o_frames = 0;

  uint8_t o_level = 0;

  init_uint8_t_array(o_gfxbuf, 512, 0xff);

  init_uint16_t_array(o_bullet, len_bullet, 0x0000);
  init_int8_t_array(o_bullet_velocity, len_bullet, 0);
  init_uint8_t_array(o_bullet_lifetime, max_bullets, 0x00);

  init_uint16_t_array(o_asteroid, len_asteroid, 0x0000);
  init_int8_t_array(o_asteroid_velocity, len_asteroid, 0);
  init_uint8_t_array(o_asteroid_diameter, max_asteroids, 0x00);
  glcd_clr();
//      add_bullet(40, 16, 3, -12, o_asteroid, o_asteroid_velocity, o_asteroid_diameter, 16);

  uint8_t more = 1;
  while(more) {
    
    unmark_bullet(o_gfxbuf, o_bullet, o_bullet_lifetime);
    unmark_asteroid(o_gfxbuf, o_asteroid, o_asteroid_diameter);
    unmark_ship(o_gfxbuf, o_ship_x, o_ship_y, o_facing);

//    if (count_asteroids(o_asteroid_diameter) == 0) more = 0;
    if (count_asteroids(o_asteroid_diameter) == 0) {
      o_level++;
      o_facing = 12;
      o_ship_x = 32 * 256;
      o_ship_y = 32 * 256;
      o_ship_xvel = 0;
      o_ship_yvel = 0;
      if (o_level == 5) {
        more = 0;
	victory_message("You Won!");
      }
      else {
        launch_level (o_asteroid, o_asteroid_velocity, o_asteroid_diameter, o_level);
      }
    }

    update_bullets(o_bullet, o_bullet_velocity, o_bullet_lifetime, 1);
    update_bullets(o_asteroid, o_asteroid_velocity, o_asteroid_diameter, 0);
    update_ship(&o_ship_x, &o_ship_y, &o_ship_xvel, &o_ship_yvel, &o_facing, o_bullet, o_bullet_velocity, o_bullet_lifetime, &o_fire_delay);
    collisions (o_ship_x, o_ship_y, o_bullet, o_bullet_velocity, o_bullet_lifetime, o_asteroid, o_asteroid_velocity, o_asteroid_diameter);

    mark_asteroid(o_gfxbuf, o_asteroid, o_asteroid_diameter);
    if (mark_ship(o_gfxbuf, o_ship_x, o_ship_y, o_facing)) {
      message("CRASH");
      more = 0;
//      print_string(0,0,"c");
    }
    mark_bullet(o_gfxbuf, o_bullet, o_bullet_lifetime);
    o_frames++;
//    if (TCCR0 > 7812) TCNT? 
    if (TCNT0 >= 128) {
      TCNT0 -= 128;
      o_timer++;
      if (o_timer > 60) {
        o_timer -=61;
        print_string (0,0, "FPS: ");
	glcd_write(85);
	glcd_write(170);
	glcd_write(0);
	glcd_write(0);
        glcd_write(o_frames);
	o_frames = 0;
      }
    }
    write_gfx(1, o_gfxbuf);
  }
}

