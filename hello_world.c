#include<avr/io.h>
// this is the header file that tells the compiler what pins and ports, etc.
// are available on this chip.
#define F_CPU 8000000
#include<util/delay.h>
#include <avr/pgmspace.h>
#include<cp437font8x8.h>

// Some macros that make the code more readable
#define output_low(port,pin) port &= ~(1<<pin)
#define output_high(port,pin) port |= (1<<pin)
#define set_input(portdir,pin) portdir &= ~(1<<pin)
#define set_output(portdir,pin) portdir |= (1<<pin)

#define CSA PC0
#define CSB PC1
#define RST PC5

#define LED PB0

#define EN PC4
#define RS PC2
#define RW PC3
#define DATA PORTD

#define display_width 192
#define display_height 64

struct rgb {
  uint8_t red;
  uint8_t green;
  uint8_t blue;
};

volatile uint16_t msec = 0;

void csa_low(void)
{
    output_low(PORTC, CSA);
}

void csa_high(void)
{
    output_high(PORTC, CSA);
}

void csb_low(void)
{
    output_low(PORTC, CSB);
}

void csb_high(void)
{
    output_high(PORTC, CSB);
}

void rs_low(void)
{
    output_low(PORTC, RS);
}

void rs_high(void)
{
    output_high(PORTC, RS);
}

void rw_low(void)
{
    output_low(PORTC, RW);
}

void rw_high(void)
{
    output_high(PORTC, RW);
}

void rst_low(void)
{
    output_low(PORTC, RST);
}

void rst_high(void)
{
    output_high(PORTC, RST);
}

void en_low(void)
{
    output_low(PORTC, EN);
}

void en_high(void)
{
    output_high(PORTC, EN);
}

void enable_pulse(void)
{
  en_high();
  _delay_us(5);
  en_low();
  _delay_us(5);
}

void glcd_on(void)
{
    //Activate both chips
    csa_low();
    csb_low();
    rs_low();
    rw_low();
    DATA = 0x3F;         //ON command
    enable_pulse();
    csb_high();
    enable_pulse();
    csa_high();
    csb_low();
    enable_pulse();
}

void glcd_off(void)
{
    //Activate both chips
    csa_high();
    csb_low();
    rs_low();
    rw_low();
    DATA = 0x3E;         //OFF command
    enable_pulse();
}

void update_timer (void) { // this calls them "msec" but they are not
  while (TCNT0 > 7) {
    msec ++;
    TCNT0 -= 8;
  }
  if (msec > 65000) msec = 0;
}

void delay_ms(int n) {
  msec = 0;
  while (msec < n) {
    update_timer();
  }
}

void init_clock (void) {
  TCCR0 |= ((1 << CS02) | (1 << CS00)); // timer at Fcpu / 1024
}

void goto_col(unsigned int x)
{
   unsigned short Col_Data;
   rs_low();
   rw_low();
   if(x<64)             //left section
   {
      csa_low();
      csb_low();
      Col_Data = x;              //put column address on data port
   }
   else if (x<128)                //middle
   {
      csa_low();
      csb_high();
      Col_Data = x-64;   //put column address on data port
   }
   else                 //right section
   {
      csa_high();
      csb_low();
      Col_Data = x-128;   //put column address on data port
   }
   Col_Data = (Col_Data | 0x40 ) & 0x7F;  //Command format
   DATA = Col_Data;
   enable_pulse();
}
 
void goto_row(unsigned int y)
{
   unsigned short Col_Data;
   rs_low();
   rw_low();
   Col_Data = (y | 0xB8 ) & 0xBF; //put row address on data port set command
   DATA = Col_Data;
   enable_pulse();
}

void goto_xy(unsigned int x,unsigned int y)
{
    goto_col(x);
    goto_row(y);
}

void glcd_write(unsigned short b)
{
   rs_high();
   rw_low();
   DATA = b;            //put data on data port
   _delay_us(1);
   enable_pulse();
}
 
unsigned short glcd_read(unsigned short column)
{
    unsigned short read_data = 0; //Read data here
    DDRD = 0x00;
    rs_high();
    rw_high();
    if (column>128){
      csa_high();
      csb_low();
    }
    else if (column>63) {
      csa_low();
      csb_high();
    }
    else {
      csa_low();
      csb_low();
    }

    _delay_us(1);        //tasu
    en_high();
    _delay_us(1);        //twl + tf
 
    //Dummy read
    en_low();
    _delay_us(5);
    en_high();
    _delay_us(1);             //tr + td(twh)
 
    read_data = DATA;    //Input data
    en_low();
    _delay_us(1);        //tdhr
    DDRD = 0xFF;
    return read_data;
}

void draw_point(unsigned short x,unsigned short y, unsigned short color)
{
    unsigned short Col_Data;;
    goto_xy(x,(y/8));
    switch (color)
    {
        case 0:         //Light spot
            Col_Data = ~(1<<(y%8)) & glcd_read(x);
        break;
        default:         //Dark spot
            Col_Data = (1<<(y%8)) | glcd_read(x);
        break;
    }
    goto_xy(x,(y/8));
    glcd_write(Col_Data);
}

void propagate(unsigned short x, unsigned short y) {
  unsigned short col_data;
//  goto_xy(x, y/8);
//  glcd_write(0xFE);
  goto_xy(x, y/8);
  col_data = glcd_read(x);
  goto_xy(x, y/8);
  glcd_write(col_data);
//  _delay_ms(10);
}

void put_char(uint8_t row, uint8_t col, uint8_t character) {
  uint16_t index = character * 8;
  goto_xy (col * 8, row);
  uint8_t i;
  for (i=0;i<8;i++) {
    glcd_write(pgm_read_byte(&cp437[index + i]));
  }
}

/*
void add_char(char character) {
  uint16_t index = character * 8;
  uint8_t i;
  for (i=0;i<8;i++) {
    glcd_write(pgm_read_byte(&cp437[index + i]));
  }
}
*/

void glcd_clrln(unsigned short ln)
{
   int i;
   goto_xy(0,ln);      //At start of line of left side
   csa_low();
   csb_low();
   for(i=0;i<65;i++)
     glcd_write(0x00);

   goto_xy(64,ln);     //At start of line of right side (Problem)
   csa_high();
   csb_low();
   for(i=0;i<65;i++)
     glcd_write(0x00);

   goto_xy(128,ln);     //At start of line of right side (Problem)
   csa_low();
   csb_high();
   for(i=0;i<65;i++)
     glcd_write(0x00);
}

void glcd_blkln(unsigned short ln)
{
   int i;
   goto_xy(0,ln);      //At start of line of left side
   csa_low();
   csb_low();
   for(i=0;i<65;i++)
     glcd_write(0xff);

   goto_xy(64,ln);     //At start of line of right side (Problem)
   csa_high();
   csb_low();
   for(i=0;i<65;i++)
     glcd_write(0xff);

   goto_xy(128,ln);     //At start of line of right side (Problem)
   csa_low();
   csb_high();
   for(i=0;i<65;i++)
     glcd_write(0xff);
}

void propagate_line(unsigned short ln) {
  int i;
  for (i=0;i<192;i++) {
    propagate(i, ln*8);
  }
//  _delay_ms(1000);
}

void propagate_all(void) {
  int i;
  for (i=0;i<8;i++) {
    propagate_line(i);
  }
}
 
//-- -----------------------
 
void glcd_clr(void)
{
   unsigned short m;
   for(m=0;m<8;m++){
    glcd_clrln(m);
   }
}

void glcd_blk(void)
{
   unsigned short m;
   for(m=0;m<8;m++){
    glcd_blkln(m);
   }
}

void set_start_line(unsigned short line)
{
    rs_low();
    rw_low();
    //Activate both chips
    csa_low();
    csb_low();
    DATA = 0xC0 | line;     //Set Start Line command
    enable_pulse();
}

void print_string(uint8_t col, uint8_t row, char s[]) {
  uint8_t finished = 0;
  uint8_t count = 0;
  while (!finished) {
    if (s[count] == '\0') finished = 1;
    else {
      goto_xy (col*8, row);
      put_char(row, col, s[count]);
      count++;
      col++;
      if (col >= display_width / 8) {
        col = 0;
	row++;
      }
    }
  }
}

int main(void) {


//  _delay_us(10000);


//  TCCR0 |= ((1 << CS02) | (1 << CS00)); // timer at Fcpu / 1024
  init_clock();

  // initialize the direction of the B port to be outputs
  // on the 3 pins that have LEDs connected

  set_output(DDRC, CSA);
  set_output(DDRC, CSB);
  set_output(DDRC, RST);
  set_output(DDRC, RS);
  set_output(DDRC, RW);
  set_output(DDRC, EN);
  
  DDRD=0xFF;
  DATA=0x00;


  set_output(DDRB, LED);  

  output_low(PORTB, LED);

  rst_low();
  enable_pulse();
  _delay_ms(15);

  rst_high();

  csa_high();
  csb_high();
  _delay_ms(1000);
  glcd_on();
  _delay_ms(1000);
  glcd_clr();
//  set_start_line(0);
  unsigned int i = 0;
  unsigned int j = 0;

  do {
//  glcd_on();
  glcd_clr();
  set_start_line(0);
//  goto_xy(0,0);
//  glcd_write(0xFF);
/*
  for (j=0;j<64;j+=1) {
    for (i=0;i<192;i+=4) {
      draw_point (i, j, 0);
//      output_high(PORTB, LED);
      _delay_ms(10);
//      GLCD_CLR();
//      output_low(PORTB, LED);
    }
  }
*/
//  propagate_all();
//  _delay_ms(600);
  i++;
  j++;
  uint8_t rows = display_height / 8;
  uint8_t columns = display_width / 8;
  if (j >= columns) j -= columns;
  if (i >= rows) i -= rows;
  print_string (j, i, "Hello World!");
/*
  _delay_ms(600);
  put_char (3, 5, 72);
  _delay_ms(200);
  put_char (3, 6, 101);
  _delay_ms(300);
  put_char (3, 7, 108);
  _delay_ms(400);
  put_char (3, 8, 108);
  _delay_ms(200);
  put_char (3, 9, 111);
  _delay_ms(200);
  put_char (3, 10, 32);
  _delay_ms(300);
  put_char (3, 11, 119);
  _delay_ms(200);
  put_char (3, 12, 111);
  _delay_ms(600);
  put_char (3, 13, 114);
  _delay_ms(200);
  put_char (3, 14, 108);
  _delay_ms(300);
  put_char (3, 15, 100);
  _delay_ms(1000);
  put_char (3, 16, 33);
*/
  _delay_ms(1600);
//  glcd_off();
  } while(1);
  /*
do {
  output_high(DDRB, LED);
  _delay_ms(500);
  output_low(DDRB, LED);
  _delay_ms(500);
}while (1);
*/
}

