#include<avr/io.h>

#define output_low(port,pin) port &= ~(1<<pin)
#define output_high(port,pin) port |= (1<<pin)
#define set_input(portdir,pin) portdir &= ~(1<<pin)
#define set_output(portdir,pin) portdir |= (1<<pin)

#define ELAPSED TCNT0
#define BUTTON0 PB0
#define BUTTON1 PB1
#define BUTTON2 PB2
#define BUTTON3 PB3
#define BUTTON4 PB4
#define BUTTON5 PB5

uint8_t button0 = 0;
uint8_t button1 = 0;
uint8_t button2 = 0;
uint8_t button3 = 0;
uint8_t button4 = 0;
uint8_t button5 = 0;

uint16_t bounce_delay = 1600;
uint16_t repeat_delay = 19200;

void reset_clock(void) {
  ELAPSED = 0;
}

void init_buttons (void) {
  TCCR0 |= ((1 << CS02) | (1 << CS00)); // timer at Fcpu / 1024
  set_input(DDRB, BUTTON0);
  set_input(DDRB, BUTTON1);
  set_input(DDRB, BUTTON2);
  set_input(DDRB, BUTTON3);
  set_input(DDRB, BUTTON4);
  set_input(DDRB, BUTTON5);
  output_high(PORTB, BUTTON0);
  output_high(PORTB, BUTTON1);
  output_high(PORTB, BUTTON2);
  output_high(PORTB, BUTTON3);
  output_high(PORTB, BUTTON4);
  output_high(PORTB, BUTTON5);
}

uint8_t button_pressed (uint8_t *b) {
/*
  if (*b == 1) {
    if (ELAPSED > bounce_delay) {
//      *b = *b + 1;
      ++*b;
      ELAPSED = 0;
      return 1; // true
    }
    else return 0; //false
  }
  else if (*b > 1) {
    if (ELAPSED > repeat_delay) {
      ELAPSED = 0;
      return 1; // true
    }
    else return 0;
  }
  else {
    ELAPSED = 0;
    *b = 1;
    return 0; //false
  }
  else return 0;
*/
  return 1; // bounce doesn't matter
}

uint8_t button0_pressed(void) {
  return (bit_is_clear (PINB, BUTTON0));
}

uint8_t button1_pressed(void) {
  return (bit_is_clear (PINB, BUTTON1));
}

uint8_t button2_pressed(void) {
  return (bit_is_clear (PINB, BUTTON2));
}

uint8_t button3_pressed(void) {
  return (bit_is_clear (PINB, BUTTON3));
}

uint8_t button4_pressed(void) {
  return (bit_is_clear (PINB, BUTTON4));
}

uint8_t button5_pressed(void) {
  return (bit_is_clear (PINB, BUTTON5));
}

